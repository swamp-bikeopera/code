-- Swamp Bike Opera embedded system for Kaffe Matthews
-- Copyright (C) 2012 Wolfgang Hauptfleisch, Dave Griffiths
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module("gps", package.seeall)

require 'posix'
require 'socket'
require 'std'

local testing=true
local testing_pos={lat=41.147387819527,lng=-8.6317899816589}
--{lat=41.140157986104,lng=-8.6173141712656}
--{lat=51.04790318580878,lng=3.7287747859954834}
--{lat=51.04803133876788,lng=3.730437755584717}
-- {lat=50.1729952601643,lng=-5.106239318847656}
-- {lat=50.16647031001818,lng=-5.097473859786987}

local test_time=0

---
-- convert wgs data to location
function wgs_to_dec(lat, sign1, lon, sign2)

    local d,m,r = string.match(lat, "(%d%d)(%d%d).(%d%d%d%d)")
    local s = ( 60 / 10000 ) * r
    local lat_deg = d + (m/60) + (s/3600)

    local d,m,r = string.match(lon, "(%d%d)(%d%d).(%d%d%d%d)")
    local s = ( 60 / 10000 ) * r
    local lon_deg = d + (m/60) + (s/3600)

    if sign2 == "W" then
       lon_deg = lon_deg * - 1
    end

    local file = io.open("/tmp/swamp_gps", "w")
    if testing then
        file:write(std.round(testing_pos.lat, 10).." "..
                   std.round(testing_pos.lng, 10))

        testing_pos.lat=testing_pos.lat+0.0001*math.cos(test_time)
        --(math.random()-0.5)*0.0001
        testing_pos.lng=testing_pos.lng+0.0001*math.sin(test_time)
        --(math.random()-0.5)*0.0001

        test_time=test_time+0.1
    else
        file:write(std.round(lat_deg, 10).." "..std.round(lon_deg, 10))
    end

    file:close()
end

--- main loop
--@param device number
function loop(device)
    for line in io.open("/dev/ttyACM"..device):lines() do
      local no, lat, sign1, lon, sign2  =
          string.match(line, "GPGGA,(.-),(.-),(%w),(.-),(%w)")
      if lat and sign1 then
          wgs_to_dec(lat, sign1, lon, sign2)
      end
    end
    return false
end

---
--@return device number or false
function detect_device()
    for i=0,20 do
       if io.open("/dev/ttyACM"..i) then
         return i
       end
    end
    return false
end


---
function startup()
    posix.mkfifo("/tmp/swamp_gps")
    while true do
        local dev = detect_device()
        if dev then
            print("found gps device "..dev)
            loop(dev)
        else
            print("no gps device found")
            os.execute("sleep 5")
        end
    end
end
